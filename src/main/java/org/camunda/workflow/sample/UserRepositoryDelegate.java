package org.camunda.workflow.sample;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserRepositoryDelegate implements JavaDelegate {

    private static final Logger log = LoggerFactory.getLogger(UserRepositoryDelegate.class);

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        log.info("Running user repository delegate.");

        log.info("x value: " + delegateExecution.getVariable("x"));
        log.info("y value: " + delegateExecution.getVariable("y"));
    }
}
