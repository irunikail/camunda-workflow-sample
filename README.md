# Camunda Workflow Sample

This is a sample created to test the camunda workflow.

## Building the project

`mvn clean install`

## Testing the workflow Deployment

- After the build a war file related to the workflow will be created under the target directory 
`target/camunda-workflow-sample.war`

- Copy `target/camunda-workflow-sample.war` to `<camunda-server>/server/apache-tomcat-9.0.19/webapps/`

- Goto camunda cockpit -> Processes. If the web app is successfully deployed there 
will be a process called `camunda-workflow-sample`.


## Testing the workflow

- Since this sample is only printing the X & Y values please tail the log file using
following command   
`tail -f <camunda-server>/server/apache-tomcat-9.0.19/logs/catalina.<date>.log` 

- Go to camunda tasklist -> start process

- Click camunda-workflow-sample -> start

- Goto All tasks 

    - click `X value` -> claim -> enter the value -> complete
    - click `Y value` -> claim -> enter the value -> complete
    
- Check the printed logs.

## References

[https://camunda.com/learn/videos/](https://camunda.com/learn/videos/)

